﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FpsCheck : MonoBehaviour
{

	string[] vsyncStr = { "Don't Sync", "Every V Blank", "Every Second V Blank" };

	#region V Sync Blank変更
	public void changeEveryVBlank()
	{
		QualitySettings.vSyncCount = 1;
	}
	public void changeEverySecondVBlank()
	{
		QualitySettings.vSyncCount = 2;
	}
	public void changeDontVsnc()
	{
		QualitySettings.vSyncCount = 0;
	}
	#endregion

	#region targetFrameRate変更
	public void changeFrameRateDefault()
	{
		Application.targetFrameRate = -1;
	}
	public void changeFrameRate10()
	{
		Application.targetFrameRate = 10;
	}
	public void changeFrameRate30()
	{
		Application.targetFrameRate = 30;
	}
	public void changeFrameRate60()
	{
		Application.targetFrameRate = 60;
	}
	public void changeFrameRate90()
	{
		Application.targetFrameRate = 90;
	}
	public void changeFrameRate120()
	{
		Application.targetFrameRate = 120;
	}
	#endregion

	int frameCount;
	float prevTime;
	float currentFps;

	void Awake()
	{
		QualitySettings.vSyncCount = 2;

        // 縦
        Screen.autorotateToPortrait = true;
        // 左
		Screen.autorotateToLandscapeLeft = false;
        // 右
		Screen.autorotateToLandscapeRight = false;
        // 上下反転
        Screen.autorotateToPortraitUpsideDown = true;
	}

	void Start()
	{
		frameCount = 0;
		prevTime = 0.0f;
		currentFps = 0.0f;
	}

	void Update()
	{
#if flase
		switch (Screen.orientation){
			// 縦画面のとき
            case ScreenOrientation.Portrait:
                // 左回転して左向きの横画面にする
                Screen.orientation = ScreenOrientation.LandscapeLeft;
                break;
            // 上下反転の縦画面のとき
            case ScreenOrientation.PortraitUpsideDown:
                // 右回転して左向きの横画面にする
                Screen.orientation = ScreenOrientation.LandscapeRight;
                break;
        }
#endif

		frameCount++;
		float time = Time.realtimeSinceStartup - prevTime;

		if(time >= 0.5f) {
			currentFps = frameCount / time;
			frameCount = 0;
			prevTime = Time.realtimeSinceStartup;
		}
	}

	void OnGUI()
    {
		// deltaTime表示
		GUI.TextArea(new Rect(10, 0, 200, 20), "deltaTime : " + Time.deltaTime);
		// fixedDeltaTime表示
        GUI.TextArea(new Rect(10, 20, 200, 20), "fixedDeltaTime : " + Time.fixedDeltaTime);

        // deltaTimeから毎フレーム計算
        float fps = Mathf.Round(1 / Time.deltaTime * 100) / 100;
		GUI.TextArea(new Rect(10, 50, 200, 20), "計算① : " + fps.ToString("F2") + " FPS");
		// 一定時間毎にFPS計算
		GUI.TextArea(new Rect(10, 70, 200, 20), "計算⑵ : " + currentFps.ToString("F2") + " FPS");

        // V Sync Count設定表示
        GUI.TextArea(new Rect(400, 0, 250, 20), "V Sync Count : " + vsyncStr[QualitySettings.vSyncCount]);
        // tagetFrameRate表示
        GUI.TextArea(new Rect(400, 20, 250, 20), "targetFrameRate : " + Application.targetFrameRate);

		// 画面向き表示
		GUI.TextArea(new Rect(10, 100, 250, 20), "Screen.orientation : " + Screen.orientation);
	}
}