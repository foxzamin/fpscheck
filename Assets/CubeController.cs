﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeController : MonoBehaviour {

	[SerializeField]
	private bool mDeltaTimeEnable;

	public void deltaTimeEnable(){
		mDeltaTimeEnable = true;
	}
	public void deltaTimeDisable()
    {
        mDeltaTimeEnable = false;
    }

	// Use this for initialization
	void Start () {
		mDeltaTimeEnable = true;	
	}

	// Update is called once per frame
	void Update () {
		if(mDeltaTimeEnable){
			transform.Rotate(new Vector3(20, 20, 0) * Time.deltaTime);
		}
		else{
			transform.Rotate(new Vector3(2, 2, 0));
		}
	}
}
